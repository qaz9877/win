package main

import (
	"fmt"
	"syscall"
	"unsafe"
)

/*位：
BYTE 8位 字节
WORD 16位
DWORD 32位
QWORD 64位
LPCWSTR   16位
内存地址的宽度：64位
*/

var (
	user32 = syscall.NewLazyDLL("User32.dll")

	gdi32 = syscall.NewLazyDLL("Gdi32.dll")

	findWindow       = user32.NewProc("FindWindowW")
	getWindowRect    = user32.NewProc("GetWindowRect")
	getDesktopWindow = user32.NewProc("GetDesktopWindow")
)

type RECT struct {
	Left, Top, Right, Bottom int32
}

func main() {
	// 获取窗口句柄
	hwnd, _, _ := findWindow.Call(0, uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr("spyxx - Everything"))))

	if hwnd <= 0 {
		fmt.Println("未找到目标窗口")
		return
	}

	// 获取窗口位置

	rect := RECT{}
	_, _, _ = getWindowRect.Call(hwnd, uintptr(unsafe.Pointer(&rect)))

	fmt.Println(rect)
	// 获取桌面窗口句柄
	desktop, _, _ := getDesktopWindow.Call()

	// 创建设备上下文
	dc, _, _ := user32.NewProc("GetDC").Call(desktop)
	defer user32.NewProc("ReleaseDC").Call(desktop, dc)

	// 创建位图并截图
	bmp, _, _ := gdi32.NewProc("CreateCompatibleBitmap").Call(dc, uintptr(rect.Right), uintptr(rect.Bottom))
	memdc, _, _ := gdi32.NewProc("CreateCompatibleDC").Call(dc)
	defer gdi32.NewProc("DeleteObject").Call(bmp)
	defer gdi32.NewProc("DeleteDC").Call(memdc)

	var oldbmp uintptr
	oldbmp, _, _ = gdi32.NewProc("SelectObject").Call(memdc, bmp)

	user32.NewProc("PrintWindow").Call(hwnd, memdc, 0)

	gdi32.NewProc("SelectObject").Call(memdc, oldbmp)

	// 将位图保存到文件
	syscall.NewLazyDLL("Gdi32.dll").NewProc("BitBlt").Call(
		dc, 0, 0,
		uintptr(rect.Right-rect.Left),
		uintptr(rect.Bottom-rect.Top),
		memdc, 0, 0, 0x00CC0020) // SRCPAINT

	image, _, _ := syscall.NewLazyDLL("Ole32.dll").NewProc("CreateStreamOnHGlobal").Call(0, 1, uintptr(unsafe.Pointer(&bmp)))
	defer syscall.NewLazyDLL("Ole32.dll").NewProc("ReleaseStream").Call(image)

	file, _ := syscall.UTF16PtrFromString("screenshot.bmp")
	syscall.NewLazyDLL("Gdiplus.dll").NewProc("GdipSaveImageToFile").Call(
		uintptr(unsafe.Pointer(image)), uintptr(unsafe.Pointer(file)), 0)
}
