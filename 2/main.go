package main

import (
	"fmt"
	"syscall"

	"github.com/sqlpxc/win"
)

func ReadProcessMemory(hProcess uintptr, lpBaseAddress uintptr, lpBuffer *uintptr, nSize uintptr, lpNumberOfBytesRead *uintptr) uint32 {

	ret, _, _ := syscall.Syscall6(readProcessMemory.Addr(), 5,
		uintptr(hProcess),
		uintptr(lpBaseAddress),
		uintptr(unsafe.Pointer(lpBuffer)),
		uintptr(nSize),
		uintptr(unsafe.Pointer(lpNumberOfBytesRead)), 0)

	return uint32(ret)
}
func WriteProcessMemory(hProcess uintptr, lpBaseAddress uintptr, lpBuffer *uintptr, nSize uintptr, lpNumberOfBytesWritten *uintptr) uint32 {

	ret, _, _ := syscall.Syscall6(writeProcessMemory.Addr(), 5,
		uintptr(hProcess),
		uintptr(lpBaseAddress),
		uintptr(unsafe.Pointer(lpBuffer)),
		uintptr(nSize),
		uintptr(unsafe.Pointer(lpNumberOfBytesWritten)), 0)

	return uint32(ret)
}

func main() {
	var pid uint32
	var data uintptr = 10000
	var address1 uintptr
	var address2 uintptr = 0x656650
	var ret uintptr
	hwdn := win.FindWindow(nil, syscall.StringToUTF16Ptr("Tutorial-i386"))
	win.GetWindowThreadProcessId(hwdn, &pid)
	ph := win.OpenProcess(0xFFF, 0, pid)
	win.ReadProcessMemory(ph, address2, &address1, 4, &ret)
	for {
		var p uintptr
		win.ReadProcessMemory(ph, address1+0x4B0, &data, 4, &ret)
		fmt.Print("当前健康值：", data, "\n")
		fmt.Print("请输出要修改的值：")
		fmt.Scan((&p))
		win.WriteProcessMemory(ph, address1+0x4B0, &p, 4, &ret)
	}
}
