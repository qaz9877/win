package main

import (
	"errors"
	"fmt"
	"unsafe"

	"golang.org/x/sys/windows"
)

var user32 = windows.NewLazySystemDLL("User32.dll")
var procfindw = user32.NewProc("FindWindowW")
var procpost = user32.NewProc("PostMessageW")
var phandle windows.Handle = 0
var pid uint32 = 0
var hwnd uint32 = 0

func main() {

	fmt.Println(GetHWNDAndPid("spyxx - Everything"))
}

func Findwinds(lpClassName *uint16, lpwindname *uint16) (ret uint32, err error) {

	// 	[in, optional] LPCWSTR lpClassName,  uint32
	//   [in, optional] LPCWSTR lpWindowName  uint32
	r0, _, err := procfindw.Call(
		uintptr(unsafe.Pointer(lpClassName)),
		uintptr(unsafe.Pointer(lpwindname)),
	)
	if r0 <= 0 {
		return 0, err
	}

	return uint32(r0), nil
}

func GetHWNDAndPid(windowsname string) (Hwnd uint32, pid uint32, err error) {
	Hwnd = 0
	pid = 0
	Hwnd, err = Findwinds(nil, windows.StringToUTF16Ptr(windowsname))
	if err != nil {
		return
	}
	// [in]            HWND    hWnd,      uint32
	// [out, optional] LPDWORD lpdwProcessId   uint32

	//r0, _, e1 := syscall.Syscall(procGetWindowThreadProcessId.Addr(),
	// 2,
	// uintptr(hwnd),
	//  uintptr(unsafe.Pointer(pid)), 0)
	tid, err := windows.GetWindowThreadProcessId(windows.HWND(Hwnd), &pid)
	if tid == 0 {
		return
	}
	return
}
func init() {
	var err error
	hwnd, pid, err = GetHWNDAndPid("扫雷")

	/*
	  HANDLE OpenProcess(
	  [in] DWORD dwDesiredAccess,   uint32
	  [in] BOOL  bInheritHandle,    bool
	  [in] DWORD dwProcessId       uint32
	);*/

	//r0, _, e1 := syscall.Syscall(procOpenProcess.Addr(),
	// 3,
	// uintptr(desiredAccess),
	//  uintptr(_p0),
	//  uintptr(processId))
	phandle, err = windows.OpenProcess(
		0x000F0000|0x00100000|0xFFFF,
		true,
		pid,
	)
	if err != nil {
		panic("获取失败")
	}

}

func GetgAME() (h int32, w int32) {
	//0x1005338
	/*
	  [in]  HANDLE  hProcess,    uint32
	  [in]  LPCVOID lpBaseAddress, uint
	  [out] LPVOID  lpBuffer,     int32
	  [in]  SIZE_T  nSize,         int32
	  [out] SIZE_T  *lpNumberOfBytesRead
	*/

	//r1, _, e1 := syscall.Syscall6(procReadProcessMemory.Addr(),
	// 5, uintptr(process),
	// uintptr(baseAddress),
	// uintptr(unsafe.Pointer(buffer)),
	// uintptr(size),
	// uintptr(unsafe.Pointer(numberOfBytesRead)), 0)

	err := windows.ReadProcessMemory(phandle,
		uintptr(0x1005338),
		(*byte)(unsafe.Pointer(&h)),
		4,
		nil)
	if err != nil {
		fmt.Println("获取游戏失败")
	}
	//0x1005334
	return
}

func GetgAMEarr() (h int32, w int32) {
	//0x1005338
	var Dlarr [24][36]byte
	err := windows.ReadProcessMemory(phandle,
		uintptr(0x1005338),
		(*byte)(unsafe.Pointer(&Dlarr[0][0])),
		1235,
		nil)
	if err != nil {
		fmt.Println("获取地雷	数组失败")
	}
	//0x1005334
	return
}

//https://learn.microsoft.com/zh-cn/windows/win32/winmsg/about-messages-and-message-queues
//              0x0201
// 0x0202
//PostMessageW
// [in, optional] HWND   hWnd, uint32
// [in]           UINT   Msg,   uint
// [in]           WPARAM wParam,   uint
// [in]           LPARAM lParam   uint32
func postmessage(hWnd uint32, mes uint, wParam uint, lParam uint32) error {
	flag, _, _ := procpost.Call(uintptr(hWnd), uintptr(mes), uintptr(wParam), uintptr(lParam))
	if flag == 0 {
		return errors.New("sss")
	}
	return nil
}

func Clik(x int16, y int16) {
	xy := [2]int16{x, y}
	err := postmessage(hwnd, 0x0201, 0x1, *((*uint32)(unsafe.Pointer(&xy[0]))))
	if err != nil {
		fmt.Println()
	}
	err = postmessage(hwnd, 0x0202, 0x1, *((*uint32)(unsafe.Pointer(&xy[0]))))
	if err != nil {
		fmt.Println()
	}
}
