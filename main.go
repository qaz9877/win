package main

import (
	"fmt"
	"syscall"
	"time"

	"github.com/lxn/win"
	"golang.org/x/sys/windows"
)

var (
	kernel32, _        = syscall.LoadLibrary("kernel32.dll")
	getModuleHandle, _ = syscall.GetProcAddress(kernel32, "GetModuleHandleW")
)

func main() {
	dd := win.FindWindow(nil, syscall.StringToUTF16Ptr("cmd"))
	fmt.Println(dd)
	hd, _ := windows.GetStdHandle(21924)

	fmt.Println(hd)
	var buf uint16 = 1
	var towrite uint32 = 1
	var written uint32 = 14
	var reserved byte = 'a'
	//func WriteConsole(console Handle, buf *uint16, towrite uint32, written *uint32, reserved *byte)
	err := windows.WriteConsole(hd, &buf, towrite, &written, &reserved)
	fmt.Println(err)
	version, err := windows.GetVersion()
	if err != nil {

	}
	fmt.Println(version)

}

func GetModuleHandle() (handle uintptr) {
	if ret, _, callErr := syscall.Syscall(getModuleHandle, 0, 0, 0, 0); callErr != 0 {
		fmt.Println("Call GetModuleHandle", callErr)
	} else {
		handle = ret
	}
	return
}

func a() {
	defer syscall.FreeLibrary(kernel32)
	time.Sleep(3 * time.Second)
}
